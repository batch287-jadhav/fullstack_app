import { Button, Row, Col} from 'react-bootstrap';
import Banner from '../components/Banner'

export default function Error() {

	const data ={
		title: "Error 404 - Page Not Found!",
		content: "The page you are looking can not be found",
		destination: "/",
		label: "Back to Home"

	}

	return(
		<Banner data={data} />
	)
}