import { Card, Button, Col, Row } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

export default function ProductCard({product}) {
	
	console.log(product)

	const { name, description, price, _id } = product;


	return (
		<Row className="mt-3 mb-3">
			<Col xs={12}>
				<Card className='cardHighlight p-0'>
					<Card.Body>
						<Card.Title><h3>{name}</h3></Card.Title>
						<Card.Subtitle>Description:</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Button className='bg-primary' as ={Link} to={`/products/${_id}`}>View</Button>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}